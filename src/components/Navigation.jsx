import React from 'react'
import { Link } from 'react-router-dom';

export default function Navigation() {
    return (
        <div className='navBar'>
            <h1>Welcome to Stephen King's library</h1>
            <div className='home'>
                <Link to="/">Home</Link>
            </div>
        </div>
    )
}
