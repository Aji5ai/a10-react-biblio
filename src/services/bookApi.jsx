// Ce fichier définit la fonction getBooks qui est utilisée pour récupérer les données des livres depuis une API 
//externe. Il utilise axios pour faire des requêtes HTTP. La fonction transforme les données reçues de l’API en un 
//format utilisable par l’application, en créant un tableau d’objets livre avec des identifiants uniques et les 
//informations nécessaires.

//On pourrait envisager d’utiliser useParams pour récupérer le paramètre ‘id’ dans le composant Details au lieu de passer l’état via Link. Cela simplifierait le passage des données MAIS nécessiterait que les informations du livre soient accessibles globalement ou via une autre requête à l’API dans le composant Details.
// Ce qui n'est pas le cas, du moins, pas ausis facileement avec cette api.

// Importation de la bibliothèque axios pour les requêtes HTTP
import axios from 'axios';

// URL de l'API pour récupérer les livres de Stephen King
const apiURL = 'https://openlibrary.org/search.json?author=stephen+king';

// Fonction getBooks pour obtenir les livres depuis l'API
export const getBooks = () => {
  // Utilisation de axios pour faire une requête GET à l'API
  return axios.get(apiURL).then((response) => 
    // Transformation des données reçues pour créer un tableau d'objets livre
    response.data.docs.map((book, index) => ({
      id: index, // Attribution d'un id basé sur l'index
      title: book.title, // Titre du livre
      pageNumber: book.number_of_pages_median, // Nombre médian de pages
      rating: book.ratings_average, // Note moyenne du livre
    }))
  );
};
