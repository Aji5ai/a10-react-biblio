//Ce fichier définit le composant Home, qui est chargé de récupérer et d’afficher la liste des livres. Il utilise les 
//hooks useState et useEffect pour gérer l’état local et les effets secondaires. Le composant récupère les données 
//des livres via une API et les stocke dans le localStorage pour éviter de refaire des appels inutiles. Il affiche 
//également une animation de chargement pendant que les données sont récupérées. Les livres sont listés avec des 
//liens (Link) qui permettent de naviguer vers la page de détails de chaque livre.

import React, { useState, useEffect } from 'react';
import { getBooks } from '../services/bookApi'; // fonction qui fait l'appel api
import { Link } from 'react-router-dom';

// Composant Home qui affiche la liste des livres avec des options de tri
export default function Home() {
  // bookList stocke la liste des livres, setBookList est la fonction pour mettre à jour cette liste
  const [bookList, setBookList] = useState(() => {
    // Tentative de récupération des livres depuis le localStorage
    const savedBooks = localStorage.getItem('books');
    // Si des livres sont sauvegardés, les parse et les retourne, sinon retourne un tableau vide
    return savedBooks ? JSON.parse(savedBooks) : [];
  });

  // isLoading indique si l'application est en train de charger des données, setIsLoading pour changer cet état
  const [isLoading, setIsLoading] = useState(!localStorage.getItem('books'));
  // loadingDots stocke les points de suspension pour l'animation de chargement
  const [loadingDots, setLoadingDots] = useState('');
  // État pour suivre le type de tri sélectionné
  const [sortType, setSortType] = useState('title');

  // Premier useEffect pour charger les livres depuis l'API si non présents dans le localStorage
  useEffect(() => {
    if (!localStorage.getItem('books')) {
      getBooks().then((books) => {
        // Mise à jour de la liste des livres et sauvegarde dans le localStorage
        setBookList(books);
        localStorage.setItem('books', JSON.stringify(books));
        // Arrêt de l'animation de chargement
        setIsLoading(false);
      });
    }
  }, []);

  // Deuxième useEffect pour gérer l'animation des points de suspension pendant le chargement
  useEffect(() => {
    let interval;
    if (isLoading) {
      // Création d'un intervalle pour ajouter des points de suspension toutes les secondes
      interval = setInterval(() => {
        setLoadingDots((prevDots) => (prevDots + '.'));
      }, 1000);
    }
    // Nettoyage de l'intervalle lors du démontage du composant
    return () => clearInterval(interval);
  }, [isLoading]);

  // Fonction pour trier les livres selon le critère sélectionné
  const sortBooks = (books, type) => {
    return books.sort((a, b) => {
      if (type === 'title') {
        return a.title.localeCompare(b.title);
      } else if (type === 'rating') {
        return b.rating - a.rating;
      } else if (type === 'pageNumber') {
        return a.pageNumber - b.pageNumber;
      }
    });
  };

  // Mise à jour de la liste des livres triée lors du changement du type de tri
  useEffect(() => {
    setBookList(sortBooks([...bookList], sortType));
  }, [sortType]);

  // Affichage conditionnel en fonction de l'état de chargement
  if (isLoading) {
    return <div>Chargement en cours des données de l'api{loadingDots}</div>;
  }

  // Rendu de la liste des livres avec des liens vers leurs détails et options de tri
  return (
    <div>
      {/* Sélecteur pour choisir le type de tri */}
      <select onChange={(e) => setSortType(e.target.value)}>
        <option value="title">Alphabétique</option>
        <option value="rating">Note</option>
        <option value="pageNumber">Nombre de pages</option>
      </select>

      <ul>
        {bookList.map((book, index) => (
          // Clé basée sur l'index car il n'y a pas de propriété id unique dans les données de l'API
          <li key={index}> 
            {/* Utilisation du composant Link pour naviguer vers la page de détails du livre */}
            <Link to={`/book/${book.id}`} state={{
              title: book.title,
              pageNumber: book.pageNumber,
              rating: book.rating
            }}>{book.title}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
