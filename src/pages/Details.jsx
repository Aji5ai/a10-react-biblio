//Le fichier Details contient le composant qui affiche les informations détaillées d’un livre spécifique. Il utilise 
//le hook useLocation pour accéder aux données du livre passées par le composant Link. Si aucun livre n’est trouvé 
//dans l’état de la location (par exemple, si l’utilisateur accède directement à l’URL sans passer par la page 
//d’accueil), le composant affiche un message d’erreur.

import React from 'react';
import { useLocation } from 'react-router-dom';

// Composant Details qui affiche les détails d'un livre spécifique
export default function Details() {
  // Utilisation du hook useLocation pour accéder aux données passées via le composant Link
  const location = useLocation();
  const book = location.state; // Récupération des données du livre

  // Gestion du cas où le livre n'est pas trouvé dans le state
  if (!book) {
    return <div>Le livre demandé n'est pas disponible par l'url. Veuillez retourner à la liste des livres et sélectionner un livre.</div>
  }

  // Rendu des détails du livre
  return (
    <div>
      <h1>{book.title}</h1>
      <p>Nombre de pages : {book.pageNumber}</p>
      {/* toFixed permet d'arrondir ici à deux décimales */}
      <p>Note moyenne : {book.rating.toFixed(2)}</p> 
    </div>
  );
}
