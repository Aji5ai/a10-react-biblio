//Ce fichier est le point d’entrée de l’application React. Il utilise le système de routage fourni par 
//react-router-dom pour naviguer entre les différentes pages de l’application. Le composant App définit la structure 
//de base de l’application, y compris la barre de navigation (Navigation) et les routes (Routes). Les routes sont 
//définies pour la page d’accueil (Home) et la page de détails (Details), avec des chemins spécifiques qui 
//déterminent quel composant doit être rendu en fonction de l’URL.

import './App.css';
import { Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Details from './pages/Details';
import Navigation from './components/Navigation';

function App() {
  return (
    // Fragment pour regrouper plusieurs éléments sans ajouter de nœud supplémentaire au DOM
    <>
      {/* Navigation est un composant qui contient les liens de navigation */}
      <Navigation />
      {/* Routes définit les différentes routes de l'application */}
      <Routes>
        {/* Route définit un chemin exact et le composant à rendre pour ce chemin */}
        <Route exact path="/" element={<Home />} />
        {/* Route pour les détails du livre avec un paramètre dynamique 'id' */}
        <Route path="/book/:id" element={<Details />} />
      </Routes>
    </>
  )
}

export default App;
